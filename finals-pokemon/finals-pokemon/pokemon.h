#pragma once
#ifndef FINALS_POKEMON_H
#define FINALS_POKEMON_H
#include <string>

using namespace std;

class Pokemon{

public:
	void bagpack();
	void PokeCenter();
	void encounter(Pokemon * enemy, int critHit);
	void expLevelUp();

	string name;
	int hp;
	int health;
	int baseDmg;
	int level;
	int exp;
	int expGain;
	int expToNxtLevel;

};



#endif