#include "pokemon.h"
#include <iostream>
#include <time.h>

void Pokemon::bagpack()
{
	cout << name << endl;
	if (hp > 0) {
		cout << "Health: " << hp << "/" << health << endl;
	}
	else {
		cout << "Health: 0 /" << health << endl;
	}
	cout << "Damage: " << baseDmg << endl;
	cout << "Level: " << level << endl;
	cout << "Exp: " << exp << "/" << expToNxtLevel << endl;
	cout << endl << endl;
}

void Pokemon::PokeCenter()
{
	hp = health;
}

void Pokemon::encounter(Pokemon * enemy, int critHit)
{
	cout << "It's " << name << " 's turn to attack " << enemy->name << endl;
	system("pause");
	if (critHit <= 80) {
		enemy->hp -= baseDmg;
		cout << enemy->name << " has been hit by " << baseDmg << " damage." << endl;
		if (enemy->hp > 0) {
			cout << enemy->name << " has " << enemy->hp << " hp left." << endl << endl;
		}
		else if (enemy->hp <= 0) {
			cout << enemy->name << " has 0 hp left." << endl << endl;
		}
	}
	else {
		cout << name << " attack missed." << endl;
	}
	cout << endl << endl;
	system("pause");
}

void Pokemon::expLevelUp()
{
	exp = exp + expGain;
	cout << name << " has gained " << expGain << "exp." << endl;
	if (exp >= expToNxtLevel) {
		health = health * (0.15 + 1);
		baseDmg = baseDmg * (0.0 + 1);
		level += 1;
		exp = exp - expToNxtLevel;
		expGain = expGain * (0.02 + 1);
		expToNxtLevel = expToNxtLevel * (0.02 + 1);
	}
}


