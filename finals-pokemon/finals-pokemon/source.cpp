#include <string>
#include <iostream>
#include "pokemon.h"
#include <time.h>

using namespace std;


int encounter() {
	int encnterChoice;
	cout << "What action would you like to take?" << endl;
	cout << "[1] Attack" << endl;
	cout << "[2] Catch" << endl;
	cout << "[3] Run Away" << endl;
	cin >> encnterChoice;
	if ((encnterChoice != 1) && (encnterChoice != 2) && (encnterChoice != 3)) {
		while ((encnterChoice != 1) && (encnterChoice != 2) && (encnterChoice != 3)) {
			cout << "Invalid Input. Try Again." << endl;
			cin >> encnterChoice;
		}
	}
	return encnterChoice;
}

int randomChance() {
	return rand() % 100 + 1;
}


int main() {
	Pokemon* pokemon = new Pokemon[15];
	(pokemon + 0)->name = "Charmander";
	(pokemon + 0)->hp = 39;
	(pokemon + 0)->health = 39;
	(pokemon + 0)->baseDmg = 19;
	(pokemon + 0)->level = 5;
	(pokemon + 0)->exp = 0;
	(pokemon + 0)->expGain = 15;
	(pokemon + 0)->expToNxtLevel = 40;

	(pokemon + 1)->name = "Balbasaur";
	(pokemon + 1)->hp = 45;
	(pokemon + 1)->health = 45;
	(pokemon + 1)->baseDmg = 17;
	(pokemon + 1)->level = 5;
	(pokemon + 1)->exp = 0;
	(pokemon + 1)->expGain = 16;
	(pokemon + 1)->expToNxtLevel = 41;

	(pokemon + 2)->name = "Squirtle";
	(pokemon + 2)->hp = 44;
	(pokemon + 2)->health = 44;
	(pokemon + 2)->baseDmg = 18;
	(pokemon + 2)->level = 5;
	(pokemon + 2)->exp = 0;
	(pokemon + 2)->expGain = 14;
	(pokemon + 2)->expToNxtLevel = 39;

	(pokemon + 3)->name = "Treecko";
	(pokemon + 3)->hp = 40;
	(pokemon + 3)->health = 40;
	(pokemon + 3)->baseDmg = 16;
	(pokemon + 3)->level = 3;
	(pokemon + 3)->exp = 0;
	(pokemon + 3)->expGain = 13;
	(pokemon + 3)->expToNxtLevel = 37;

	(pokemon + 4)->name = "Charmander";
	(pokemon + 4)->hp = 39;
	(pokemon + 4)->health = 39;
	(pokemon + 4)->baseDmg = 17;
	(pokemon + 4)->level = 5;
	(pokemon + 4)->exp = 0;
	(pokemon + 4)->expGain = 15;
	(pokemon + 4)->expToNxtLevel = 40;

	(pokemon + 5)->name = "Scorbunny";
	(pokemon + 5)->hp = 43;
	(pokemon + 5)->health = 43;
	(pokemon + 5)->baseDmg = 17;
	(pokemon + 5)->level = 5;
	(pokemon + 5)->exp = 0;
	(pokemon + 5)->expGain = 14;
	(pokemon + 5)->expToNxtLevel = 40;

	(pokemon + 6)->name = "Chimchar";
	(pokemon + 6)->hp = 44;
	(pokemon + 6)->health = 44;
	(pokemon + 6)->baseDmg = 19;
	(pokemon + 6)->level = 7;
	(pokemon + 6)->exp = 0;
	(pokemon + 6)->expGain = 16;
	(pokemon + 6)->expToNxtLevel = 42;

	(pokemon + 7)->name = "Ralts";
	(pokemon + 7)->hp = 28;
	(pokemon + 7)->health = 28;
	(pokemon + 7)->baseDmg = 13;
	(pokemon + 7)->level = 1;
	(pokemon + 7)->exp = 0;
	(pokemon + 7)->expGain = 12;
	(pokemon + 7)->expToNxtLevel = 33;

	(pokemon + 8)->name = "Piplup";
	(pokemon + 8)->hp = 53;
	(pokemon + 8)->health = 53;
	(pokemon + 8)->baseDmg = 20;
	(pokemon + 8)->level = 8;
	(pokemon + 8)->exp = 0;
	(pokemon + 8)->expGain = 18;
	(pokemon + 8)->expToNxtLevel = 46;

	(pokemon + 9)->name = "Torchic";
	(pokemon + 9)->hp = 45;
	(pokemon + 9)->health = 45;
	(pokemon + 9)->baseDmg = 17;
	(pokemon + 9)->level = 5;
	(pokemon + 9)->exp = 0;
	(pokemon + 9)->expGain = 14;
	(pokemon + 9)->expToNxtLevel = 38;

	(pokemon + 10)->name = "Turtwig";
	(pokemon + 10)->hp = 55;
	(pokemon + 10)->health = 55;
	(pokemon + 10)->baseDmg = 19;
	(pokemon + 10)->level = 9;
	(pokemon + 10)->exp = 0;
	(pokemon + 10)->expGain = 17;
	(pokemon + 10)->expToNxtLevel = 44;

	(pokemon + 11)->name = "Fennekin";
	(pokemon + 11)->hp = 40;
	(pokemon + 11)->health = 40;
	(pokemon + 11)->baseDmg = 13;
	(pokemon + 11)->level = 4;
	(pokemon + 11)->exp = 0;
	(pokemon + 11)->expGain = 11;
	(pokemon + 11)->expToNxtLevel = 34;

	(pokemon + 12)->name = "Rattata";
	(pokemon + 12)->hp = 30;
	(pokemon + 12)->health = 30;
	(pokemon + 12)->baseDmg = 19;
	(pokemon + 12)->level = 9;
	(pokemon + 12)->exp = 0;
	(pokemon + 12)->expGain = 18;
	(pokemon + 12)->expToNxtLevel = 44;

	(pokemon + 13)->name = "Froakie";
	(pokemon + 13)->hp = 41;
	(pokemon + 13)->health = 41;
	(pokemon + 13)->baseDmg = 21;
	(pokemon + 13)->level = 6;
	(pokemon + 13)->exp = 0;
	(pokemon + 13)->expGain = 15;
	(pokemon + 13)->expToNxtLevel = 40;

	(pokemon + 14)->name = "Litten";
	(pokemon + 14)->hp = 45;
	(pokemon + 14)->health = 45;
	(pokemon + 14)->baseDmg = 19;
	(pokemon + 14)->level = 6;
	(pokemon + 14)->exp = 0;
	(pokemon + 14)->expGain = 14;
	(pokemon + 14)->expToNxtLevel = 38;

	int bag = 0;
	int storage = 100;
	Pokemon * bagPack = new Pokemon[storage];
	Pokemon * tempArr = new Pokemon[storage];

	string playerName;
	int choice;
	int pokeEnemy = 0;
	srand((unsigned int)time(NULL));
	
	cout << R"(

$$$$$$$\   $$$$$$\   $$$$$$\  $$$$$$$\  $$$$$$$\   $$$$$$\   $$$$$$\  
$$  __$$\ $$  __$$\ $$  __$$\ $$  __$$\ $$  __$$\ $$  __$$\ $$  __$$\ 
$$ |  $$ |$$ /  $$ |$$ /  \__|$$ |  $$ |$$ |  $$ |$$ /  \__|\__/  $$ |
$$$$$$$\ |$$$$$$$$ |\$$$$$$\  $$$$$$$  |$$$$$$$  |$$ |$$$$\  $$$$$$  |
$$  __$$\ $$  __$$ | \____$$\ $$  ____/ $$  __$$< $$ |\_$$ |$$  ____/ 
$$ |  $$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |      
$$$$$$$  |$$ |  $$ |\$$$$$$  |$$ |      $$ |  $$ |\$$$$$$  |$$$$$$$$\ 
\_______/ \__|  \__| \______/ \__|      \__|  \__| \______/ \________|
                                                                      
                                                                      
                                                                      
$$$$$$$$\ $$$$$$\ $$\   $$\  $$$$$$\  $$\       $$$$$$\               
$$  _____|\_$$  _|$$$\  $$ |$$  __$$\ $$ |     $$  __$$\              
$$ |        $$ |  $$$$\ $$ |$$ /  $$ |$$ |     $$ /  \__|             
$$$$$\      $$ |  $$ $$\$$ |$$$$$$$$ |$$ |     \$$$$$$\               
$$  __|     $$ |  $$ \$$$$ |$$  __$$ |$$ |      \____$$\              
$$ |        $$ |  $$ |\$$$ |$$ |  $$ |$$ |     $$\   $$ |             
$$ |      $$$$$$\ $$ | \$$ |$$ |  $$ |$$$$$$$$\\$$$$$$  |             
\__|      \______|\__|  \__|\__|  \__|\________|\______/              
                                                                      
                                                                 )" << endl << endl;
	system("pause");
	system("cls");

	cout << "Hello, my name is Professor Oak. " << endl;
	cout << "May I ask for your name?" << endl;
	cin >> playerName;
	system("cls");
	cout << "Choose your starter Pokemon." << endl;
	cout << "[1] Charmander - Level 5" << endl;
	cout << "[2] Squirtle - Level 5" << endl;
	cout << "[3] Balbasaur - Level 5" << endl;
	cin >> choice;
	if (choice == 1) {
		(bagPack + 0)->name = (pokemon + 0)->name;
		(bagPack + 0)->hp = (pokemon + 0)->hp;
		(bagPack + 0)->health = (pokemon + 0)->health;
		(bagPack + 0)->baseDmg = (pokemon + 0)->baseDmg;
		(bagPack + 0)->level = (pokemon + 0)->level;
		(bagPack + 0)->exp = (pokemon + 0)->exp;
		(bagPack + 0)->expGain = (pokemon + 0)->expGain;
		(bagPack + 0)->expToNxtLevel = (pokemon + 0)->expToNxtLevel;
	}
	else if (choice == 2) {
		(bagPack + 0)->name = (pokemon + 2)->name;
		(bagPack + 0)->hp = (pokemon + 2)->hp;
		(bagPack + 0)->health = (pokemon + 2)->health;
		(bagPack + 0)->baseDmg = (pokemon + 2)->baseDmg;
		(bagPack + 0)->level = (pokemon + 2)->level;
		(bagPack + 0)->exp = (pokemon + 2)->exp;
		(bagPack + 0)->expGain = (pokemon + 2)->expGain;
		(bagPack + 0)->expToNxtLevel = (pokemon + 2)->expToNxtLevel;
	}
	else if (choice == 3) {
		(bagPack + 0)->name = (pokemon + 1)->name;
		(bagPack + 0)->hp = (pokemon + 1)->hp;
		(bagPack + 0)->health = (pokemon + 1)->health;
		(bagPack + 0)->baseDmg = (pokemon + 1)->baseDmg;
		(bagPack + 0)->level = (pokemon + 1)->level;
		(bagPack + 0)->exp = (pokemon + 1)->exp;
		(bagPack + 0)->expGain = (pokemon + 1)->expGain;
		(bagPack + 0)->expToNxtLevel = (pokemon + 1)->expToNxtLevel;
	}
	system("cls");
	bag += 1;
	(bagPack + 0)->bagpack();
	int limit = 0;
	int x = 0;
	int y = 0;
	char direct = 'k';
	int encnterChoice;
	int critHit;
	int catchRate;
	int wildPokeEncnter;

	cout << "Alright, you are all set for an adventure." << endl;
	system("pause");
	system("cls");
	for (;;) {
		if(((x <= 2 && x >= -1) && (y <= 1 && y >= -1)) || ((x <= 5 && x >= 3) && (y <= 4 && y >= 2)) || ((x <= -2 && x >= -4) && (y <= 4 && y >= 2)) || ((x <= -2 && x >= -4) && (y <= -2 && y >= -4)) || ((x <= 5 && x >= 3) && (y <= -2 && y >= -4))){
			cout << "What would you like to do now?" << endl;
			cout << "[1] Walk" << endl;
			cout << "[2] Pokemons" << endl;
			cout << "[3] Pokemon Center" << endl;
			cin >> choice;
			if (choice > 3 || choice == 0) {
				while (choice > 3 || choice == 0) {
					cout << "Invalid Input. Try Again." << endl;
					cin >> choice;
				}
			}
		}
		else if (((x <= 5 && x >= 3) && (y <= 1 && y >= -1)) || ((x <= -2 && x >= -4) && (y <= 1 && y >= -1)) || ((x <= 2 && x >= -1) && (y <= 4 && y >= 2)) || ((x <= 2 && x >= -1) && (y <= -2 && y >= -4)) || ((x > 5 || x < -4 || y > 4 || y < -4))) {
			cout << "What would you like to do now?" << endl;
			cout << "[1] Walk" << endl;
			cout << "[2] Pokemons" << endl;
			cin >> choice;
			if (choice > 2 || choice == 0) {
				while (choice > 2 || choice == 0) {
					cout << "Invalid Input. Try Again." << endl;
					cin >> choice;
				}
			}
		}
		
		system("cls");
		if (choice == 1) {
			cout << "Choose a direction." << endl;
			cout << "[A] Left" << endl;
			cout << "[S] Down" << endl;
			cout << "[W] Up" << endl;
			cout << "[D] Right" << endl;
			cin >> direct;
			while ((direct != 'a') && (direct != 's') && (direct != 'd') && (direct != 'w')) {
				cout << "Invalid Input. Try Again." << endl;
				cin >> direct;
			}
			switch (direct) {
			case 'a':
				x -= 1;
				cout << "You are now at position (" << x << "," << y << ") " << endl;
				break;
			case 's':
				y -= 1;
				cout << "You are now at position (" << x << "," << y << ") " << endl;
				break;
			case 'd':
				x += 1;
				cout << "You are now at position (" << x << "," << y << ") " << endl;
				break;
			case 'w':
				y += 1;
				cout << "You are now at position (" << x << "," << y << ") " << endl;
				break;
			}

			if ((x <= 2 && x >= -1) && (y <= 1 && y >= -1)) {
				cout << "You are currently in Pallet Town." << endl;
			}
			else if ((x <= 5 && x >= 3) && (y <= 4 && y >= 2)) {
				cout << "You are currently in Veridian City." << endl;
			}
			else if ((x <= -2 && x >= -4) && (y <= 4 && y >= 2)) {
				cout << "You are currently in Pewter City." << endl;
			}
			else if ((x <= -2 && x >= -4) && (y <= -2 && y >= -4)) {
				cout << "You are currently in Cerulean City." << endl;
			}
			else if ((x <= 5 && x >= 3) && (y <= -2 && y >= -4)) {
				cout << "You are currently in Lavender Town." << endl;
			}
			else if ((x <= 5 && x >= 3) && (y <= 1 && y >= -1)) {
				cout << "You are currently in Route 1." << endl;
			}
			else if ((x <= -2 && x >= -4) && (y <= 1 && y >= -1)) {
				cout << "You are currently in Route 2." << endl;
			}
			else if ((x <= 2 && x >= -1) && (y <= 4 && y >= 2)) {
				cout << "You are currently in Route 3." << endl;
			}
			else if ((x <= 2 && x >= -1) && (y <= -2 && y >= -4)) {
				cout << "You are currently in Route 4." << endl;
			}
			else if ((x > 5 || x < -4 || y > 4 || y < -4)) {
				cout << "You are currently in Unknown Area" << endl;
			}
			system("pause");
			system("cls");
			if (((x <= 5 && x >= 3) && (y <= 1 && y >= -2)) || ((x <= -2 && x >= -4) && (y <= 1 && y >= -1)) || ((x <= 2 && x >= -1) && (y <= 4 && y >= 2)) || ((x <= 2 && x >= -1) && (y <= -2 && y >= -4)) || ((x > 5 || x < -4 || y > 4 || y < -4))) {
				pokeEnemy = rand() % 14;
				cout << "You have encountered a Wild Pokemon: " << endl;
				(pokemon + pokeEnemy)->bagpack();
				cout << endl;
				while ((pokemon + pokeEnemy)->hp >= 0 || (bagPack + limit)->hp >= 0) {
					encnterChoice = encounter();
					system("cls");
						if (encnterChoice == 1) {
							if ((bagPack + limit)->hp >= 0) {
								critHit = randomChance();
								(bagPack + limit)->encounter((pokemon + pokeEnemy), critHit);
								if ((pokemon + pokeEnemy)->hp >= 0) {
									critHit = randomChance();
									(pokemon + pokeEnemy)->encounter((bagPack + limit), critHit);
									system("cls");
									if ((bagPack + limit)->hp <= 0) {
										cout << (bagPack + limit)->name << " got fainted." << endl;
										system("pause");
										limit++;
									}
									if (limit >= bag) {
										cout << "All your Pokemons have fainted." << endl;
										cout << "Revive them in the Pokemon center." << endl;
										system("pause");
										limit = 0;
										(pokemon + pokeEnemy)->hp = (pokemon + pokeEnemy)->health;
										break;
									}
								}
								else if ((pokemon + pokeEnemy)->hp <= 0) {
									cout << (pokemon + pokeEnemy)->name << " got defeated." << endl;
									(bagPack + limit)->expLevelUp();
									system("pause");
									limit = 0;
									(pokemon + pokeEnemy)->hp = (pokemon + pokeEnemy)->health;
									break;
								}
							}
							else if ((bagPack + limit)->hp <= 0) {
								limit++;
								if (limit >= bag) {
										cout << "All your Pokemons have fainted." << endl;
										cout << "Revive them in the Pokemon center." << endl;
										system("pause");
										limit = 0;
										break;
								}
							}
						}

						else if (encnterChoice == 2) {
							catchRate = randomChance();
							cout << "Throwing PokeBall!" << endl;
							for (int i = 0; i < 3; i++) {
								cout << ". . ." << endl;
								system("pause");
							}
							if (catchRate <= 30) {

								cout << "You caught a " << (pokemon + pokeEnemy)->name << endl;
								if (bag < storage) {
									(bagPack + bag)->name = (pokemon + pokeEnemy)->name;
									(bagPack + bag)->hp = (pokemon + pokeEnemy)->hp;
									(bagPack + bag)->health = (pokemon + pokeEnemy)->health;
									(bagPack + bag)->baseDmg = (pokemon + pokeEnemy)->baseDmg;
									(bagPack + bag)->level = (pokemon + pokeEnemy)->level;
									(bagPack + bag)->exp = (pokemon + pokeEnemy)->exp;
									(bagPack + bag)->expGain = (pokemon + pokeEnemy)->expGain;
									(bagPack + bag)->expToNxtLevel = (pokemon + pokeEnemy)->expToNxtLevel;
									bag += 1;
									system("pause");
									system("cls");
								}
								else if (bag >= storage) {
									cout << "Sorry, bag is full." << endl;
									system("pause");
									system("cls");
								}
								break;
							}
							else {
								cout << "Oh, crude!" << endl;
								cout << "It got a away." << endl;
								system("pause");
								system("cls");
							}
						}
						else if (encnterChoice == 3) {
							cout << "Okay. That seems kinda like a good idea." << endl;
							cout << "Run to safety!" << endl;
							system("pause");
							break;
							system("cls");
						}
				}
			}
		}
		else if (choice == 2) {
			for (int i = limit; i < bag; i++) {
				(bagPack + i)->bagpack();
			}
			system("pause");
			system("cls");
		}
		else if (choice == 3) {
			cout << "Hello, welcome to the Poke Center." << endl;
			cout << "Would you like to heal your Pokemons." << endl;
			cout << "(y/n)" << endl;
			cin >> direct;
			if (direct == 'y') {
				for (int i = 0; i < bag; i++) {
					(bagPack + i)->PokeCenter();
					(bagPack + i)->bagpack();
					cout << endl << endl;
				}
				system("pause");
				system("cls");
			}
			else if (direct == 'n') {
				system("cls");
			}
			cout << "Thank you. Come again" << endl;
			system("pause");
		}
		system("cls");
	}




	
}